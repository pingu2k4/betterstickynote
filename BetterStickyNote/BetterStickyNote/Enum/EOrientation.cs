﻿using BetterStickyNote.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterStickyNote.Enum
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    enum EOrientation
    {
        [Description("Centred")]
        CENTRED,
        [Description("Centred Column")]
        CENTRED_COLUMN,
        [Description("Left")]
        LEFT,
        [Description("Right")]
        RIGHT
    }
}
