﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterStickyNote.Enum
{
    enum EState
    {
        CLOSED,
        OPEN,
        EDIT
    }
}
