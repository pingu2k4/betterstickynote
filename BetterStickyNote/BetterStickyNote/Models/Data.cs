﻿using BetterStickyNote.Enum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterStickyNote.Models
{
    class Data : INPC
    {
        #region PROPERTIES
        private string _Initials;
        public string Initials
        {
            get
            {
                return _Initials;
            }
            set
            {
                _Initials = value;
                OnPropertyChanged("Initials");
            }
        }

        private EOrientation _Orientation;
        public EOrientation Orientation
        {
            get
            {
                return _Orientation;
            }
            set
            {
                _Orientation = value;
                OnPropertyChanged("Orientation");
            }
        }

        private ObservableCollection<BaseDataObject> _Fields = new ObservableCollection<BaseDataObject>();
        public ObservableCollection<BaseDataObject> Fields
        {
            get
            {
                return _Fields;
            }
            set
            {
                _Fields = value;
                OnPropertyChanged("Fields");
            }
        }

        internal void SubscribeToCollection()
        {
            foreach (var field in Fields)
            {
                field.PropertyChanged -= OnFieldPropertyChanged;
                field.PropertyChanged += OnFieldPropertyChanged;
            }
        }

        private void OnFieldPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            int index = Fields.IndexOf(sender as BaseDataObject);
            OnPropertyChanged("Fields[" + index + "]." + e.PropertyName);
        }
        #endregion
    }
}
