﻿using GlobalHotKey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterStickyNote.Models
{
    class TextData : BaseDataObject
    {
        public TextData()
        {
            RegisterHotKey();
            hkManager.KeyPressed += OnHKPressed;
        }

        #region METHODS
        private void RegisterHotKey()
        {
            if (hk != null)
                hkManager.Unregister(hk);
            if(HotKey != null)
                hk = hkManager.Register(HotKey.Key, HotKey.Modifiers);
        }
        #endregion

        #region EVENTS
        public event EventHandler HKPressed;

        private void OnHKPressed(object sender, KeyPressedEventArgs e)
        {
            HKPressed?.Invoke(this, new EventArgs());
        }
        #endregion

        #region VARIABLES
        private HotKeyManager hkManager = new HotKeyManager();
        private HotKey hk;
        #endregion

        #region PROPERTIES
        private string _Label;
        public string Label
        {
            get
            {
                return _Label;
            }
            set
            {
                _Label = value;
                OnPropertyChanged("Label");
            }
        }

        private string _Value;
        public string Value
        {
            get
            {
                return _Value;
            }
            set
            {
                _Value = value;
                OnPropertyChanged("Value");
            }
        }

        private Hotkey _HotKey;
        public Hotkey HotKey
        {
            get
            {
                return _HotKey;
            }
            set
            {
                _HotKey = value;
                OnPropertyChanged("HotKey");
                RegisterHotKey();
            }
        }
        #endregion
    }
}
