﻿using BetterStickyNote.Commands;
using BetterStickyNote.Enum;
using BetterStickyNote.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WindowsInput;

namespace BetterStickyNote.ViewModels
{
    class MainWindowViewModel : VMBase
    {
        public MainWindowViewModel()
        {
            if (Properties.Settings.Default.NeedUpgrade)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.NeedUpgrade = false;

                Properties.Settings.Default.Save();
            }

            if (string.IsNullOrEmpty(Properties.Settings.Default.Data))
                Data = new Data();
            else
                Data = JsonConvert.DeserializeObject<Data>(Properties.Settings.Default.Data, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Objects });
            Data.PropertyChanged += OnDataPropertyChanged;
            Data.Fields.CollectionChanged += OnFieldCollectionChanged;
            Data.SubscribeToCollection();
            SubscribeToAllHKEvents();
        }

        public MainWindowViewModel(Window mainWindow) : this()
        {
            this.mainWindow = mainWindow;
        }

        #region METHODS
        public void OnBobbleClicked()
        {
            switch (State)
            {
                case EState.CLOSED:
                    State = EState.OPEN;
                    break;
                default:
                    System.Windows.Forms.MessageBox.Show("ERROR: Expected state to be closed.");
                    break;
            }
        }

        private void SubscribeToAllHKEvents()
        {
            foreach (var field in Data.Fields)
            {
                TextData td = field as TextData;
                if(td != null)
                {
                    td.HKPressed -= OnHotkeyPressed;
                    td.HKPressed += OnHotkeyPressed;
                }
            }
        }

        internal void WindowClosing(CancelEventArgs e)
        {
            Properties.Settings.Default.WindowTop = WindowTop;
            Properties.Settings.Default.Save();
        }

        public override void Dispose()
        {

        }
        #endregion

        #region EVENTS
        private void OnDataPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Properties.Settings.Default.Data = JsonConvert.SerializeObject(Data, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Objects });
            Properties.Settings.Default.Save();
        }

        private void OnFieldCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Properties.Settings.Default.Data = JsonConvert.SerializeObject(Data, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Objects });
            Properties.Settings.Default.Save();

            Data.SubscribeToCollection();

            SubscribeToAllHKEvents();
        }

        private void OnHotkeyPressed(object sender, EventArgs e)
        {
            if(State != EState.EDIT)
            {
                inputSimulator.Keyboard.TextEntry((sender as TextData).Value);
            }
        }
        #endregion

        #region PROPERTIES
        private double _WindowTop = Properties.Settings.Default.WindowTop;
        public double WindowTop
        {
            get
            {
                return _WindowTop;
            }
            set
            {
                _WindowTop = value;
                OnPropertyChanged("WindowTop");
            }
        }

        private double _WindowLeft = -25;
        public double WindowLeft
        {
            get
            {
                return _WindowLeft;
            }
            set
            {
                _WindowLeft = value;
                OnPropertyChanged("WindowLeft");
            }
        }

        private EState _State = EState.CLOSED;
        public EState State
        {
            get
            {
                return _State;
            }
            set
            {
                EState previousState = _State;
                _State = value;
                OnPropertyChanged("State");
                switch (value)
                {
                    case EState.CLOSED:
                        WindowWidth = 50;
                        WindowLeft = -25;
                        break;
                    case EState.OPEN:
                        WindowWidth = 350;
                        if (previousState == EState.CLOSED && WindowLeft < 10)
                            WindowLeft = 10;
                        break;
                    case EState.EDIT:
                        WindowWidth = 350;
                        break;
                    default:
                        break;
                }
            }
        }

        private double _WindowWidth = 50;
        public double WindowWidth
        {
            get
            {
                return _WindowWidth;
            }
            set
            {
                _WindowWidth = value;
                OnPropertyChanged("WindowWidth");
            }
        }

        private Data _Data;
        public Data Data
        {
            get
            {
                return _Data;
            }
            set
            {
                _Data = value;
                OnPropertyChanged("Data");
            }
        }
        #endregion

        #region VARIABLES
        Window mainWindow;
        InputSimulator inputSimulator = new InputSimulator();
        #endregion

        #region COMMANDS
        private ICommand _CloseInfo;
        public ICommand CloseInfo
        {
            get
            {
                if (_CloseInfo == null)
                {
                    _CloseInfo = new RelayCommand(CloseInfoEx, null);
                }
                return _CloseInfo;
            }
        }
        private void CloseInfoEx(object p)
        {
            if (State == EState.EDIT)
                State = EState.OPEN;
            else
                State = EState.CLOSED;
        }

        private ICommand _EditInfo;
        public ICommand EditInfo
        {
            get
            {
                if (_EditInfo == null)
                {
                    _EditInfo = new RelayCommand(EditInfoEx, null);
                }
                return _EditInfo;
            }
        }
        private void EditInfoEx(object p)
        {
            if (State == EState.EDIT)
                State = EState.OPEN;
            else
                State = EState.EDIT;
        }

        private ICommand _AddSeperator;
        public ICommand AddSeperator
        {
            get
            {
                if (_AddSeperator == null)
                {
                    _AddSeperator = new RelayCommand(AddSeperatorEx, null);
                }
                return _AddSeperator;
            }
        }
        private void AddSeperatorEx(object p)
        {
            Data.Fields.Add(new Seperator());
        }

        private ICommand _AddText;
        public ICommand AddText
        {
            get
            {
                if (_AddText == null)
                {
                    _AddText = new RelayCommand(AddTextEx, null);
                }
                return _AddText;
            }
        }
        private void AddTextEx(object p)
        {
            Data.Fields.Add(new TextData());
        }

        private ICommand _AddLargeText;
        public ICommand AddLargeText
        {
            get
            {
                if (_AddLargeText == null)
                {
                    _AddLargeText = new RelayCommand(AddLargeTextEx, null);
                }
                return _AddLargeText;
            }
        }
        private void AddLargeTextEx(object p)
        {
            Data.Fields.Add(new LargeTextData());
        }

        private ICommand _RemoveElement;
        public ICommand RemoveElement
        {
            get
            {
                if (_RemoveElement == null)
                {
                    _RemoveElement = new RelayCommand(RemoveElementEx, null);
                }
                return _RemoveElement;
            }
        }
        private void RemoveElementEx(object p)
        {
            Data.Fields.Remove(p as BaseDataObject);
        }

        private ICommand _MoveElementUp;
        public ICommand MoveElementUp
        {
            get
            {
                if (_MoveElementUp == null)
                {
                    _MoveElementUp = new RelayCommand(MoveElementUpEx, null);
                }
                return _MoveElementUp;
            }
        }
        private void MoveElementUpEx(object p)
        {
            BaseDataObject bdo = p as BaseDataObject;
            int index = Data.Fields.IndexOf(bdo);

            if (index > 0)
            {
                Data.Fields.Move(index, index - 1);
            }
        }

        private ICommand _MoveElementDown;
        public ICommand MoveElementDown
        {
            get
            {
                if (_MoveElementDown == null)
                {
                    _MoveElementDown = new RelayCommand(MoveElementDownEx, null);
                }
                return _MoveElementDown;
            }
        }
        private void MoveElementDownEx(object p)
        {
            BaseDataObject bdo = p as BaseDataObject;
            int index = Data.Fields.IndexOf(bdo);

            if (index < Data.Fields.Count - 1)
            {
                Data.Fields.Move(index, index + 1);
            }
        }

        private ICommand _Copy;
        public ICommand Copy
        {
            get
            {
                if (_Copy == null)
                {
                    _Copy = new RelayCommand(CopyEx, null);
                }
                return _Copy;
            }
        }
        private void CopyEx(object p)
        {
            Clipboard.SetText(p as string);
        }
        #endregion
    }
}
