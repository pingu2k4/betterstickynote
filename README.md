##**Better Sticky Notes**

Better Sticky Notes is a better solution to windows sticky notes. It provides unlimited text fields, with optional labels and a seperate edit and view mode.

# Features
* Always on top! Stays on top of all other windows.
* Collapses to a tiny circle when not needed to stay out the way.
* Customisable Initials
* Create and view unlimited fields, consisting of label, value, hotkey
* Click any field and its value will be inserted to your clipboard, so you can easily paste it somewhere
* Add short text fields, long text fields, or seperators and re-order them however you please
* 4 different orientations to choose from
* Press a hotkey for a corresponding field and the value will be sent to the currently active application as a string!
* Dark Mode (the only mode)

# Collapsed Mode
In collapsed mode, it will collapse to a small circle, and automatically dock itself on the left side of your window. 

![Bobble](https://bitbucket.org/pingu2k4/betterstickynote/raw/1627e3a66c08c0be5fae238965650bedbf1def2b/Images/Bobble.png)

# View Mode
When you click the circle it will expand into view mode. In view mode, you can see all your fields and seperators, with their listed labels.
In this mode, if you click any of the fields then it will briefly flash to confirm, and copy the value to your clipboard so it can easily be pasted elsewhere.

![View Mode](https://bitbucket.org/pingu2k4/betterstickynote/raw/1627e3a66c08c0be5fae238965650bedbf1def2b/Images/ViewMode.png)

# Edit Mode
Finally, clicking the E button will take you to edit mode. In edit mode, you can change the 2 characters shown, the orientation used, and completely edit all fields and their order.
Fields have a label, value and a hotkey. If you set a hotkey, when you press the given hotkey whilst not in edit mode it will send the characters for the value of that field to whichever applicatio you currently have open. For example, if you have notepad open and you trigger a hotkey, it will write out the value of the related field into notepad.
All fields can be reordered with ease, and new ones can be added at the click of a button.

![Edit Mode](https://bitbucket.org/pingu2k4/betterstickynote/raw/1627e3a66c08c0be5fae238965650bedbf1def2b/Images/EditMode.png)